public class Genie {

    private int maxWishes;
    private int grantWish=maxWishes;

    public Genie (int maxWishes){
        this.maxWishes=maxWishes;
    }

    public int getMaxWishes() {
        return maxWishes;
    }

    public int grantWish(){
        for (int i=0;i<maxWishes;i++){
            if (grantWish==0){
                System.out.println("No more wishes");
            } else {
                grantWish=grantWish-1;
            }
        }
        return grantWish;

    }


}
