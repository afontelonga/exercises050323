public class MagicLamp {

    private int maxNumber;
    private int genies=maxNumber;

    private String genieType="Friendly";
    public MagicLamp (int maxNumber){
        this.maxNumber=maxNumber;
    }
    public int getMaxNumber() {
        return maxNumber;
    }

    public int geniesLeft(){
        for (int i=0; i<getMaxNumber();i++){
            genies=genies-1;
        }
        return genies;
    }

    public String callDemon(){
        if (genies==0){
            genieType="Demon";
        }
        return genieType;
    }


}
