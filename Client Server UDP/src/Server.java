import java.io.IOException;
import java.io.Reader;
import java.net.*;
import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Server {
    public static void main(String[] args) throws IOException {
        InetAddress hostName= InetAddress.getByName("localhost");
        int portN=6000;

        byte[] rBuffer=new byte [1024];

        DatagramSocket socket=new DatagramSocket(portN);
        DatagramPacket messageR=new DatagramPacket(rBuffer, rBuffer.length);
        socket.receive(messageR);
        String message= new String(messageR.getData());
        System.out.println(message);

        String messageToUpper=message.toUpperCase();
        byte[] sBuffer=messageToUpper.getBytes();
            DatagramPacket sendP=new DatagramPacket(sBuffer, sBuffer.length, hostName,6050);
            socket.send(sendP);
        socket.close();
    }
}