import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {

        InetAddress hostName= InetAddress.getByName("localhost");
        int portN=6050;

        System.out.println("Write something");
        Scanner darkly=new Scanner(System.in);
        String s= darkly.nextLine();

        byte[] sBuffer=s.getBytes();
        DatagramSocket socket=new DatagramSocket(portN);
        DatagramPacket sendP=new DatagramPacket(sBuffer, sBuffer.length, hostName,6000);
        socket.send(sendP);

        byte[] rBuffer=new byte[1024];
        DatagramPacket messageR= new DatagramPacket(rBuffer, rBuffer.length);
        socket.receive(messageR);
        String s1= new String(messageR.getData());

        System.out.println(s1);
        socket.close();
    }
}