import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class ServerWorker implements Runnable {
    LinkedList<ServerWorker> clientsList = new LinkedList<>();
    private String name;
    private int maxClient = 10;
    private int activeClient;
    private PrintWriter sending;
    private BufferedReader receiving;
    Socket pointOfCommunication;
    {
        try {
            pointOfCommunication = new Socket("localhost", 40000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public ServerWorker(String name, Socket pointOfCommunication){
        this.name=name;
        this.pointOfCommunication=pointOfCommunication;
        try {
            receiving = new BufferedReader(new InputStreamReader(pointOfCommunication.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public String getName() {
        return name;
    }
    @Override
    public void run() {
        System.out.println("Thread "+Thread.currentThread().getName()+"started");
                while (pointOfCommunication.isConnected()) {
                    System.out.println("testing here");

                    try {
                        String messageReceived = null;
                        messageReceived = receiving.readLine();
                        if (messageReceived!=null) {
                            System.out.println("the client sent this message: " + messageReceived);
                            System.out.println("and here");
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
        }
}

