import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client{
    static String name;
    static Socket pointOfCommunication;
    int portNumber=60001;
    static PrintWriter sending;
    static BufferedReader receiving;
        InetAddress hostName;
    {
        try {
            hostName = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
    public Client () {

        try {
            pointOfCommunication=new Socket("localhost",40000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] args) {
        ClientWorker clientWorker=new ClientWorker();
        Client client = new Client();
        Thread theOneWhoReadsIncomingMessages=new Thread(clientWorker);
        theOneWhoReadsIncomingMessages.start();
        receiving = new BufferedReader(new InputStreamReader(System.in));
        try {
            sending = new PrintWriter(pointOfCommunication.getOutputStream(), true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        while ((client.pointOfCommunication.isConnected())) {
            Scanner darkly = new Scanner(System.in);
            String sendingMessage = darkly.nextLine();
            System.out.println(client + " sent: " + sendingMessage);
            sending.write(sendingMessage);
        }
        }
    public void closing(){
        try {
            receiving.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        sending.close();
        try {
            pointOfCommunication.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}