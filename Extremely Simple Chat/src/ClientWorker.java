import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientWorker implements Runnable{

    private String threadName;
    Socket pointOfCommunication ;
    int portNumber=60001;
    InetAddress hostName;
    {
        try {
            hostName = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
    public ClientWorker(){
        try {
            pointOfCommunication = new Socket("localhost",40000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void run() {
        ClientWorker clientWorker=new ClientWorker();
        BufferedReader receiving;
        try {
            receiving = new BufferedReader(new InputStreamReader(pointOfCommunication.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        while (pointOfCommunication.isConnected()) {
            String messageReceived = null;
            try {
                messageReceived = receiving.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            System.out.println(messageReceived);
        }
    }
}
