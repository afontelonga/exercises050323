import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Server {
    private final static int port = 40000;
    private List<ServerWorker> workers = Collections.synchronizedList(new ArrayList<ServerWorker>());

    public static void main(String[] args) {
        Socket pointOfCommunication;
        ServerSocket listening;
        Server server = new Server();

        {
            try {
                listening = new ServerSocket(port);
                System.out.println("The server is listening!");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        while (true) {
            try {
                pointOfCommunication = listening.accept();
                System.out.println("Client accepted! " + pointOfCommunication);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            int connectionCount = 0;
            //create a new Server Worker
            connectionCount++;
            String name = "Client";
            ServerWorker serverWorker = new ServerWorker(name, pointOfCommunication);
            server.workers.add(serverWorker);

            //serve the client connection with a new Thread
            Thread theOneWhoReadsIncomingMessages = new Thread(serverWorker);
            theOneWhoReadsIncomingMessages.setName("s1 ");
            theOneWhoReadsIncomingMessages.start();

            PrintWriter sending;
            BufferedReader receiving;

            while (pointOfCommunication.isConnected()) {

                try {

                    sending = new PrintWriter(pointOfCommunication.getOutputStream(), true);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                try {
                    receiving = new BufferedReader(new InputStreamReader(pointOfCommunication.getInputStream()));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                String sendingMessage = null;
                try {
                    sendingMessage = receiving.readLine();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                System.out.println(sendingMessage);
                sending.write(sendingMessage);

            }
        }
    }
}
