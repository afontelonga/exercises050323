import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] nums = {1, 2, 4, 9, 5, 3, 8, 7, 10, 12, 14};
        int i = 0;
        int nummax;
        for (i = 0; i < nums.length; i++) {
            nummax = nums[0];
            if ((i % 2) == 0) {
                for (int j = 1; j < nums.length;) {
                    if (nummax > nums[i]) {
                        nums[i] = nummax;
                        j++;
                    } else {
                        nummax = nums[j];
                        nums[i] = nummax;
                        j++;
                    }
                }
            } else {
                i++;
            }
        }
        System.out.println(Arrays.toString(nums));
    }
}