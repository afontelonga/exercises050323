public class Main {
    public static void main(String[] args) {
        int arr[] = {5, 7, -8, 5, 14, 2, 10, 3, 20};


        int secMin=0;
        int min=0;
        for (int i=0; i< arr.length;i++){
            min=arr[0];
            secMin=arr[0];
            for (int j=1;j< arr.length;j++){
                if (arr[j]<min) {
                    min = arr[j];
                }
                if ((arr[j]<secMin) && (secMin!=min)){
                    secMin=arr[j];
                }
            }
        }
        System.out.println("The min is : " + min);
        System.out.println("The second min is: " + secMin);
    }
}