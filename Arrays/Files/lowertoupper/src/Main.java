import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

        FileReader fReader = new FileReader("resources/readme_pt.txt");
        BufferedReader bReader = new BufferedReader(fReader);

        FileWriter fWriter = new FileWriter("resources/copyreadme.txt");
        BufferedWriter bWriter = new BufferedWriter(fWriter);

        int i;
        String line = "";
        do {
            line = bReader.readLine();
            if (line != null) {

                    bWriter.write(line.toUpperCase()+"\n");
                }

            }while (line!=null);
        fReader.close();
        fWriter.close();
    }
}