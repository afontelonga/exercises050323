import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) throws IOException {
            boolean debug = true;

            System.out.print("Name of file: ");
            String nameFile = new Scanner(System.in).nextLine();
            System.out.print("Letter for count: ");
            String letter = new Scanner(System.in).nextLine();

            FileReader myFile = new FileReader(nameFile);;
            BufferedReader myfileBufferedReader = new BufferedReader(myFile);


            String line;
            int countLetter = 0;
            do {
                line = myfileBufferedReader.readLine();
                if (line != null) {
                    for (int i = 0; i < line.length(); i++) {
                        if (line.substring(i, i + 1).equals(letter)) {
                            countLetter++;
                        }
                    }
                }
            } while (line!=null);
            myFile.close();

            System.out.printf("Amount of letter: %1$s" + "\r\n", countLetter);

        }
    }

