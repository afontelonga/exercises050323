import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        FileReader file= null;
        try {
            file = new FileReader("resources/text.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        BufferedReader bfile=new BufferedReader(file);

        String line= "";
        try {
            line= bfile.readLine();
            if (line!= null){
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            bfile.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}