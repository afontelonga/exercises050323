import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

        FileReader fReader = new FileReader("resources/readme_pt - Cópia (2).txt");
        BufferedReader bReader = new BufferedReader(fReader);

        FileWriter fWriter = new FileWriter("resources/censorship");
        BufferedWriter bWriter = new BufferedWriter(fWriter);


        String line = "";

        do {
            line = bReader.readLine();
            if (line != null) {
                    line = line.replaceAll("ficheiro", "censored");
                    bWriter.write(line + "\n");

            }
        } while (line != null);
        fReader.close();
        fWriter.close();
    }
}
/*
        FileReader fReader2 = new FileReader("resources/censorship");
        BufferedReader bReader2 = new BufferedReader(fReader);

        String lineC= " ";

        do {
            lineC= bReader2.readLine();
            if (lineC!=null){
                if (lineC.contains("ficheiro")){
                   lineC= lineC.replace("ficheiro","censored" );
                    bWriter.write(lineC);
                }

            }

        }while (lineC!=null);

        }

*/


