import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Logger {

    static void Write(String s){

        try {
            OutputStreamWriter file=new FileWriter("resources/text.txt");
            if (s.length()!=0){
                file.write(s);
            }
            file.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

}
