import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        String sentence= "I'm super cool";
        FileWriter myFile= null;
        try {
            myFile = new FileWriter("resources/test.txt");
        } catch (IOException e) {
            throw new RuntimeException("File not found");
        }

        if (sentence.length()!=0){
            try {
                myFile.write(sentence);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        }

        try {
            myFile.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}