import java.io.*;

public class ReplaceWords {

    static void replaceWordsInFile(String wordsR, String wordsToR) throws IOException {
        FileReader fileReader = new FileReader("resources/text.txt");
        BufferedReader bfileReader = new BufferedReader(fileReader);
        FileWriter fileWriter = new FileWriter("resources/text.txt.out");

        String line = "";

        line = bfileReader.readLine();
        if (line != null) {
            line=line.replace(wordsR, wordsToR);
            fileWriter.write(line);
        }

            fileReader.close();
            fileWriter.close();

        }
    }

