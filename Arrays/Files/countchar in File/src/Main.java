import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        FileReader fileR= new FileReader("resources/readme_pt.txt");
        BufferedReader bFileR= new BufferedReader(fileR);


        String character="a";
        String line=" ";


        int countLetter=0;
        do {
            line= bFileR.readLine();
            if (line != null) {

                for (int i = 0; i < line.length(); i++) {
                    if (line.substring(i, i + 1).equals(character)) {
                        countLetter++;

                    }
                }
            }
        }while (line!=null);
        fileR.close();
        System.out.println("O caracter " + character+" aparece " + countLetter + " vezes");



    }
}