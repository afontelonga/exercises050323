import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

            FileInputStream fileToCopy=new FileInputStream("resources/istockphoto-182802778-170667a.jpg");
            FileOutputStream copyFile= new FileOutputStream("resources/copypict.jpeg");
            byte [] buffer= new byte[1024];

            while ((fileToCopy.read(buffer))!=-1){
                copyFile.write(buffer);
            }

        fileToCopy.close();
        copyFile.close();

    }
}