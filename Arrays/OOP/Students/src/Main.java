public class Main {
    public static void main(String[] args) {
        Person person=new Person();
        Student student=new Student();
        Teacher teacher=new Teacher();

        System.out.println(person.Hello("Ana"));
        System.out.println(student.ShowAge(person.SetAge(21)));

        System.out.println(person.Hello("Teresa"));
        System.out.println("I am " + person.SetAge(30));
        System.out.println(teacher.Explain());


    }
}