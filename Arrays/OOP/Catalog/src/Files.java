public class Files {

    private String name;
    private int code;
    private String category;
    private int size;

    public Files(String name, int code, String category, int size){
        this.name=name;
        this.code= code;
        this.category= category;
        this.size=size;
    }

    public String getName() {
        return name;
    }
    public int getCode() {
        return code;
    }
    public String getCategory() {
        return category;
    }
    public int getSize() {
        return size;
    }

    public void Play(){

    }
    public void RetrieveInformation(){

    }
}
