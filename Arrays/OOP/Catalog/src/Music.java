public class Music extends Files{

    private String singer;
    private int length;
    public Music(String name, int code, String category, int size, String singer, int length) {
        super(name, code, category, size);
        this.singer=singer;
        this.length=length;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getCode() {
        return super.getCode();
    }

    @Override
    public String getCategory() {
        return super.getCategory();
    }

    @Override
    public int getSize() {
        return super.getSize();
    }

    public String getSinger() {
        return singer;
    }

    public int getLength() {
        return length;
    }

    @Override
    public void Play() {
        super.Play();
    }

    @Override
    public void RetrieveInformation() {
        super.RetrieveInformation();
    }
}
