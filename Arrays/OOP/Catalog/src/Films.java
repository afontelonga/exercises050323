public class Films extends Files{

    private String director;
    private String mainActor;
    private String mainActress;

    public Films(String name, int code, String category, int size,String director,String mainActor,String mainActress) {
        super(name, code, category, size);
        this.director=director;
        this.mainActor=mainActor;
        this.mainActress=mainActress;
    }
    @Override
    public String getName() {
        return super.getName();
    }
    @Override
    public int getCode() {
        return super.getCode();
    }
    @Override
    public String getCategory() {
        return super.getCategory();
    }
    @Override
    public int getSize() {
        return super.getSize();
    }

    public String getDirector() {
        return director;
    }

    public String getMainActor() {
        return mainActor;
    }

    public String getMainActress() {
        return mainActress;
    }
}
