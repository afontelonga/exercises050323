public class PhotoAlbum {

    private int numberOfPages;

    public PhotoAlbum(int numberOfPages) {
        this.numberOfPages=numberOfPages;
    }


    public int GetNumberOfPages(){
        return numberOfPages;
    }

    @Override
    public String toString() {
        return numberOfPages+" ";
    }
}
