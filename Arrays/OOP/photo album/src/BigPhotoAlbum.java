public class BigPhotoAlbum {

    private int numberOfPages=64;

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public String toString() {
        return numberOfPages+" ";
    }
}
