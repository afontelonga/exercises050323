public class House {

    private int area;
    Door door;

    public House (int area){
        this.area=area;
        door=new Door();
    }
    public int getArea() {
        return area;
    }
    public void setArea(int value) {
        area = value;
    }
    public Door getDoor(){
        return door;
    }
    public void setDoor(Door value){
        door=value;
    }
    public void ShowData(){
        System.out.println("My house area is: " + area);
    }

}
