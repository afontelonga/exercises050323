public class Door {

    private String color;

    public Door (){
        color="Brown";
    }
    public Door (String color){
        this.color=color;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String value) {
        color = value;
    }
    public void ShowData (){
        System.out.println("My color of my door is " + color);
    }
}
