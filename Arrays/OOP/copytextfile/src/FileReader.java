import java.io.BufferedReader;
import java.util.Iterator;

public class FileReader implements Iterable<String>{

        private final String filename;

        public FileReader(String filename) {
            this.filename = filename;
        }

    @Override
    public Iterator<String> iterator() {
        return new FileReaderIterator();
    }
    private  class FileReaderIterator implements Iterator<String>{

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public String next() {
            return null;
        }
    }
}
