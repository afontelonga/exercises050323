public class Main {
    public static void main(String[] args) {
        String s="abcdefghijklmnopqrstuvwxy";
        int slength=s.length();
        int equalParts=slength/5;
        String s1=s.substring(0,equalParts);
        String s2=s.substring(equalParts, (equalParts*2));
        String s3= s.substring((equalParts*2),(equalParts*3));
        String s4=s.substring((equalParts*3),(equalParts*4));
        String s5=s.substring(equalParts*4);

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
        System.out.println(s5);

    }
}