import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        String s= "Reverse words in a given string";
        String[] newS= s.split(" ");

        int length= newS.length;

        for (int i=0;i< newS.length-1;i++){
            String temp= newS[i];
            newS[i]=newS[length-1];
            newS[length-1]=temp;
            length--;
        }

        System.out.println(Arrays.toString(newS));




    }
}