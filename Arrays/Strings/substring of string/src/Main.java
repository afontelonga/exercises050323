import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String s="w3resource";
        String [] phrase=new String[s.length()];
        for (int i=0;i<s.length();i++){
            for (int j=1;j<s.length();j++){
                if (s.charAt(i)!=s.charAt(j)){
                    phrase[i]= String.valueOf(s.charAt(i));
                } else {
                    phrase[i]= String.valueOf(s.charAt(j)+1);
                }
            }
        }
        System.out.println(Arrays.toString(phrase));
    }
}