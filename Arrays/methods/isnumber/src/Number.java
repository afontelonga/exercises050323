public class Number {

    static int[] numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    static boolean IsNumber(String text) {

        for (int i = 0; i < text.length(); i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (text.charAt(i) != numbers[j]) {
                    return false;
                }
            }
        } return true;
    }
}
