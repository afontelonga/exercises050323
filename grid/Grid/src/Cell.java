import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {
    private Rectangle cell;
    static final int cellSize=35;
    static final int padding=10;
    int col;
    int row;

    int x;
    int y;

    public Cell(int col,int row){
        this.col=col;
        this.row=row;
        x=padding+(col*cellSize);
        y=padding+(row*cellSize);
        this.cell=new Rectangle(x,y, cellSize, cellSize);
        this.cell.draw();
    }

    public Rectangle getCell() {
        return cell;
    }


}
