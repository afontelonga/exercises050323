import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.concurrent.Callable;

public class Grid {

    private int rows;
    private int cols;

    private Cell [][] grid;

    public Grid(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
        this.grid=new Cell[cols][rows];
    }

    public void populateGrid () {
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                grid[col][row]=new Cell(col,row);
            }
        };

    }
}






