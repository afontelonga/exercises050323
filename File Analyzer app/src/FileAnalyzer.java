import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileAnalyzer {
    private BufferedReader bReader;

    {
        try {
            bReader = new BufferedReader(new FileReader("resources/Text.txt"));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    String words;

    {
        try {
            words = bReader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        FileAnalyzer countW=new FileAnalyzer();
        int i=10;
        countW.readLines();
        System.out.println("The file has: " + countW.countWords() + " words");
        System.out.println("The first word with more than " +i+ " letters is: "+ countW.firstWordLongerThan(i));
        System.out.println(countW.longestWords(i));
    }

    public void readLines(){
        Stream <String> readL=words.lines();

        readL.forEach(System.out::println);
    }
    public long countWords() {
        String[] newWords = words.split(" ");
        Stream.of(newWords)
                .count();

        return Arrays.stream(newWords).count();
    }

    public String firstWordLongerThan(int n){
       String [] listWords=words.split(" ");
       List <String> oneWord= Arrays.stream(listWords)
                .filter(word->word.length()>n)
                .collect(Collectors.toList());

       String firstWord=oneWord.get(0);
return firstWord;
    }
    public void longestWords(int x){
        String [] listWords=words.split(" ");
        List <String> longestWord= Arrays.stream(listWords)
                .filter(word->word.length()>x)
                .collect(Collectors.toList());
        
    }
    public void commonWords(){

    }


}