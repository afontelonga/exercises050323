import java.net.InetAddress;
import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {

        InetAddress hostName = InetAddress.getByName("localhost");
        int portNumber = 60001;
        Socket cSocket = new Socket(hostName, 60000);
        PrintWriter out = new PrintWriter(cSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(cSocket.getInputStream()));

        System.out.println("Chat away");
        Boolean communication = true;
        while (communication == true) {

            Scanner darkly = new Scanner(System.in);
            String iSpeak = darkly.nextLine();
            if (iSpeak != null) {
                out.println(iSpeak);
                String serverSpeaks = in.readLine();
                System.out.println(serverSpeaks);
            } else if(iSpeak.equals("quit")) {
                communication = false;
                out.close();
                in.close();
            }

        }
        out.close();
        in.close();
        cSocket.close();
    }
}
