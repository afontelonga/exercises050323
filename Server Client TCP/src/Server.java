import java.io.*;
import java.net.*;
import java.util.Scanner;
public class Server {

    public static void main(String[] args) throws IOException {


        int portNumber = 60000;
        ServerSocket serverSocket = new ServerSocket(portNumber);
        Socket clientSocket = serverSocket.accept();
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        Boolean communication = true;

        while (communication == true) {
            String clientSpeaks = in.readLine();
            if (clientSpeaks != null) {
                System.out.println("The client says: " + clientSpeaks);
                out.println(clientSpeaks);
            } else if (clientSpeaks.equals("quit")){
                communication = false;
                in.close();
                out.close();
            }
        }
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();
    }
}
